import './js/script.js'
import './css/style.css'
import './styl/style.styl'
import './styl/main.styl'


window.Vue = require('vue');
import store from './store/'
import axios from 'axios'
import { assertSpreadProperty } from 'babel-types';


Vue.component('main_component', require('./components/main_component.vue').default)

document.addEventListener('DOMContentLoaded', function() {
    window.app = new Vue({
        store,
        el: '#app',
    })
}, false);