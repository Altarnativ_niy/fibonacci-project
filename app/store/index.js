import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import AdminStatus from './admin.js'

export default new Vuex.Store({
    modules: {
        AdminStatus,
    }
})