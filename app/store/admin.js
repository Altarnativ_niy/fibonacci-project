export default {
    state: {
        isAdmin: false
    },
    actions: {
        setAdmin ({commit}, stat) {
           commit('SET_STATUS', stat)
        }
    },
    mutations: {
        SET_STATUS(state, status) {
            state.isAdmin = status
        }
     },
    getters: {
        getAdminStatus (state) {
            return state.isAdmin
        }
    }
}