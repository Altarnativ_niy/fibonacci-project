<?php 

$numAr = json_decode(file_get_contents('php://input'), true);

if(!empty($numAr)) {
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/assets/files/numbers.json', json_encode($numAr));

    echo 'success';
}
else
    echo 'error';