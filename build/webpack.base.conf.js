const path = require('path')
const fs = require('fs')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin')

const PATHS = {
  app: path.join(__dirname, '../app'),
  dist: path.join(__dirname, '../dist'),
  assets: 'assets/'
}

/** Options for PUG */
const PAGES_DIR = `${PATHS.app}/pug/pages/`
const PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith('.pug'))

module.exports = {
  externals: {
    paths: PATHS
  },
  entry: {
    app: PATHS.app,
  },
  output: {
    filename: `${PATHS.assets}js/[name].js`,
    path: PATHS.dist,
    publicPath: '/'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          name: 'vendors',
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node-modules/'
      },
      {
        test: /\.pug$/,
        loader: 'pug-loader',
        exclude: '/node-modules/'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.styl$/,
        use: [
			    'style-loader',
        	MiniCssExtractPlugin.loader, 
			    'css-loader',
			    {
			      loader: 'stylus-loader',
				    options: { }
			    },
			    //'vue-style-loader',
        ],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader, 
          'css-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      },
      {
        test: /\.html$/,
        //include: path.resolve(__dirname, 'app'),
        use: ['raw-loader'],
      },
    ],
  },
  resolve: {
	alias: {
		'vue$': 'vue/dist/vue.js'
	}
  },
  plugins: [
	  new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: `${PATHS.assets}css/[name].css`,
    }),
    new CopyWebpackPlugin([
      {
        from: `${PATHS.app}/img`,
        to: `${PATHS.assets}img`,
      },
      {
        from: `${PATHS.app}/static/`,
        to: `${PATHS.assets}files/`,
      },
      {
        from: `${PATHS.app}/ajax/`,
        to: `${PATHS.assets}ajax/`,
      },
    ]),
    ...PAGES.map(page => new HtmlWebpackPlugin({
      template: `${PAGES_DIR}/${page}`,
      filename: `./${(page == 'index.pug') ? '' : page.replace(/\.pug/, '')+'/'}index.html`,
      inject: false,
      excludeAssets: [/personal.*.js/],
      hash: false,
      minify: false
    })),
    new HtmlWebpackExcludeAssetsPlugin(),
     /*new HtmlWebpackPlugin({
      hash: false,
      template: `${PAGES_DIR}/index.pug`,
      filename: './index.html',
    }),*/
  ],
};